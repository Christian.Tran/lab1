package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {
        // TODO: Implement Rock Paper Scissors
        boolean play = true;
        
        do{
            System.out.println("Let's play round " + roundCounter);
            boolean True = true;
            String userchoice = null;
            while(True){
                userchoice = readInput("Your choice (Rock/Paper/Scissors)?");
                if(rpsChoices.contains((userchoice))){
                    break;
                }
                else{
                    System.out.println("I do not understand cardboard. Could you try again?\n");
                }
    
            }
            getwinner(userchoice);
            System.out.printf("Score: human %d, computer %d\n",humanScore,computerScore);
            String keep = readInput("Do you wish to continue playing? (y/n)?");
            if (keep.equals("n")){
                System.out.println("Bye bye :)");
                play = false;
            }
            roundCounter += 1;
        }
        while(play);        

    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {    
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }



    public void getwinner(String human){
        Random rand = new Random();
        int computer = rand.nextInt(3);
        String choice = rpsChoices.get(computer);
        if(human.equals("rock")){
            if (choice.equals("scissors")){
                System.out.printf("Human chose %s, computer chose %s. Human wins!\n",human,choice);
                humanScore += 1;
            } 
            else if (choice.equals("paper")){
                System.out.printf("Human chose %s, computer chose %s. Computer wins!\n",human,choice);
                computerScore += 1;
            } 
            else{
                System.out.printf("Human chose %s, computer chose %s. It´s a tie.\n",human,choice);   
            }         
        }
        else if(human.equals("paper")){
            if (choice.equals("scissors")){
                System.out.printf("Human chose %s, computer chose %s. Computer wins!\n",human,choice);
            }
            else if (choice.equals("rock")){
                System.out.printf("Human chose %s, computer chose %s. Human wins!\n",human,choice);
            }
            else{
                System.out.printf("Human chose %s, computer chose %s. It´s a tie.\n",human,choice);
            }
        }
        else if(human.equals("scissors")){
            if (choice.equals("rock")){
                System.out.printf("Human chose %s, computer chose %s. Computer wins!\n",human,choice);
            }
            else if (choice.equals("paper")){
                System.out.printf("Human chose %s, computer chose %s. Human wins!\n",human,choice);
            }
            else{
                System.out.printf("Human chose %s, computer chose %s. It´s a tie.\n",human,choice);
            }
        }

    }
    


}
